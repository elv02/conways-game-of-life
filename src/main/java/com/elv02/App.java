package com.elv02;

import java.awt.Frame;
import java.awt.Button;

/**
 * Hello world!
 *
 */
public class App extends Frame
{
    public App(){
        Button b = new Button("click me");
        b.setBounds(30, 100, 80, 30);
        this.add(b);
        this.setSize(1024, 768);
        this.setLayout(null);
        this.setVisible(true);
    }
    public static void main( String[] args )
    {
        /*System.out.println( "Welcome to Conway's Game of Life!" );
        int[][] presetBoard =  {{0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 0},
                                {0, 0, 0, 1, 1, 1, 0, 0, 0},
                                {0, 0, 0, 0, 1, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 0, 0, 0, 0, 0, 0, 0}};
        Conway presetGame = new Conway(presetBoard);
        int[][] resBoard = presetGame.simulate(11, 1);
        presetGame.printBoard(resBoard);*/

        Conway randGame = new Conway(2500, 2500, 0.6);
        final long startTime = System.currentTimeMillis();
        int[][] resBoard = randGame.simulate(1024, 12);
        final long elapsedTimeMillis = System.currentTimeMillis() - startTime;
        System.out.println("Processing randomized board of size " + resBoard.length + "x" + resBoard[0].length + " took " + elapsedTimeMillis + " milliseconds.");
    }
}
