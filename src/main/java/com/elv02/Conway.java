package com.elv02;

import java.util.Arrays;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.color.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

public class Conway {
    /*
     *  An instance of Conway's Game of Life.
     */
    private int[][] currBoard;
    private int[][] oldBoard;
    private int width;
    private int height;
    private JFrame mainWindow;
    private BoardCanvas gameCanvas;

    public Conway(int[][] startingBoard){
        /*
         * Starting board will be used for calculations.
         */
        this.oldBoard = copyBoard(startingBoard);
        this.currBoard = copyBoard(startingBoard);
        this.height = startingBoard.length;
        this.width = startingBoard[0].length;
        SwingUtilities.invokeLater(this::buildGUI);
    }

    public Conway(int width, int height, double chance){
        /*
         * Create an instance with board of width x and height y will be used for calculations.
         */
        this.currBoard = genBoard(width, height, chance);
        this.oldBoard = copyBoard(this.currBoard);
        this.width = width;
        this.height = height;
        SwingUtilities.invokeLater(this::buildGUI);
    }

    public int[][] simulate(int steps, int threadCount){
        /*
         * Run the simulation for n steps using m threads.
         * Returns the final state of the board.
         */
        Thread[] threads = new Thread[threadCount];
        Worker[] workers = new Worker[threadCount];

        if(threadCount > this.height){
            System.err.println("Cannot split board rows for threading. Number of threads must be equal to or greater than number of rows in board.");
            return new int[0][0];
        }

        int colSize = height/threadCount;
        int extra = height%threadCount;

        for(int i = 0; i < threadCount; i++){
            int start = i * colSize;
            int end = start + colSize;
            if(extra>0) end += extra; 
            workers[i] = new Worker(start, end);
        }

        for(int i = 0; i < steps; i++){
            // Init threads
            for(int j = 0; j < threadCount; j++){
                threads[j] = new Thread(workers[j]);
                threads[j].start();
            }
            // Wait for all to finish
            for(int j = 0; j < threadCount; j++){
                try{
                    threads[j].join();
                } catch(InterruptedException e){
                    System.err.println("Exception " + e);
                }
            }
            // Update old board
            this.oldBoard = copyBoard(this.currBoard);
            if(this.gameCanvas != null)
                this.gameCanvas.refreshCanvas();
        }
        return copyBoard(this.currBoard);
    }

    public int[][] copyBoard(int[][] toCopy){
        /*
         * Copy a game board to a new instance (for mutability)
         * Sourced from: https://stackoverflow.com/a/53397359
         */
        return Arrays.stream(toCopy).map(int[]::clone).toArray(int[][]::new);
        // End of sourced code
    }

    public void printBoard( int[][] board ){
        System.out.println("Printing board state:");
        System.out.println("=====================");
        for(int y = 0; y < board.length; y++){
            for(int x = 0; x < board[0].length; x++){
                System.out.print(board[y][x]);
            }
            System.out.println("");
        }
        System.out.println("=====================");
    }

    private int[][] genBoard(int width, int height, double chance){
        int[][] board = new int[height][width];

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                board[y][x] = chance <= Math.random() ? 1 : 0;
            }
        }

        return board;
    }

    private void buildGUI() {
        System.out.println("Creating GUI in EDT? " + SwingUtilities.isEventDispatchThread());
        this.mainWindow = new JFrame("Conway's Game of Life");
        this.mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.gameCanvas = new BoardCanvas();
        this.mainWindow.add(gameCanvas);
        this.mainWindow.pack();
        this.mainWindow.setVisible(true); 
    }

    private class BoardCanvas extends JPanel {
        public BoardCanvas(){
            setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, Color.DARK_GRAY));
        }

        @Override
        public Dimension getPreferredSize(){
            return new Dimension(800, 600);
        }

        @Override
        public void paintComponent(Graphics g){
            super.paintComponent(g);

            // Draw Text Example
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, 800, 600);
            g.setColor(Color.BLUE);
            for(int y = 0; y < currBoard.length; y++){
                for(int x = 0; x < currBoard[0].length; x++){
                    // TODO: Insert grid drawing here, determine offsets etc
                    if(currBoard[y][x] != 0){
                        g.fillRect(x*2, y*2, 2, 2);
                    }
                }
            }
        }

        public void refreshCanvas(){
            /*
             * Update the canvas when the board changes
             */
            this.repaint();
            // TODO: Adjust for possible window changes etc?
        }
    }

    private class Worker implements Runnable{
        /*
         * A thread worker that handles processing of a given horizontal slice of a game board.
         */

        private int startCol;
        private int endCol;

        public Worker(int startCol, int endCol){
            /*
             * Worker will calculate the new state for all cells in columns within in a given range. This
             * is based on the state of the old board, and changes are written to its area of the new board.
             */
            this.startCol = startCol;
            this.endCol = endCol;
        }

        @Override
        public void run(){
            for(int y = startCol; y < endCol; y++){
                for(int x = 0; x < width; x++){
                    currBoard[y][x] = cellSurvives(x, y) ? 1: 0;
                }
            }
        }

        private boolean cellSurvives(int x, int y){
            /*
            * Checks if a given cell at a position should survive based on amount of neighbors (using old board).
            */

            if(x < 0 || y < 0 || x >= width || y >= height) return false;

            int neighbors = countNeighbors(x, y);
            boolean cellAlive = oldBoard[y][x] != 0;

            if(cellAlive){
                if(neighbors < 2) return false;
                if(neighbors <= 3) return true;
                if(neighbors > 3) return false;
            } else {
                return neighbors == 3;
            }
            return false;
        }

        private int countNeighbors(int x, int y){
            /*
             * Return the amount of alive neighbors a given cell has (using old board).
             */
            int lowX = x - 1;
            int highX = x + 1;
            int lowY = y - 1;
            int highY = y + 1;
    
            int count = 0;
    
            if(lowX < 0) lowX++;
            if(lowY < 0) lowY++;
            if(highX >= width) highX--;
            if(highY >= height) highY--;
    
            for(int tY = lowY; tY <= highY; tY++){
                for(int tX = lowX; tX <= highX; tX++){
                    if(tX == x && tY == y) continue;
                    if(oldBoard[tY][tX] != 0) count++;
                }
            }
    
            return count;
        }
    }
}
